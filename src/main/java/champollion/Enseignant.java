package champollion;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Enseignant extends Personne {

    private final Set<Intervention> planification = new HashSet<>();

    // Implémentation par une Map
    private final Map<UE, ServicePrevu> enseignements = new HashMap<>();

    public Enseignant(String nom, String email) {
        super(nom, email);
    }

    public void ajouteIntervention(Intervention inter) {
        if (!enseignements.containsKey(inter.getMatiere())) {
            throw new IllegalArgumentException("La matière ne fait pas partie des enseignements");
        }

        if (inter.getDuree() > resteAPlanifier(inter.getMatiere(), inter.getType())) {
            throw new IllegalArgumentException("Il n'y a plus d'heures à planifier pour cet intervention");
        }

        planification.add(inter);
    }

    public int heuresPlanifiee() {
        float result = 0f;
        for (Intervention inter : planification) {
            result += equivalentTD(inter.getType(), inter.getDuree());
        }

        return Math.round(result);
    }

    private float equivalentTD(TypeIntervention type, int volumeHoraire) {
        // Advanced switch depuis java 14
        float result = switch (type) {
            case CM -> volumeHoraire * 1.5f;
            case TD -> volumeHoraire;
            case TP -> volumeHoraire * 0.75f;
        };
        return result;
    }

    public boolean enSousService() {
        return heuresPrevues() < 192;
    }


    /**
     * Calcule le nombre total d'heures prévues pour cet enseignant en "heures équivalent TD" Pour le calcul : 1 heure de
     * cours magistral vaut 1,5 h "équivalent TD" 1 heure de TD vaut 1h "équivalent TD" 1 heure de TP vaut 0,75h "équivalent
     * TD"
     *
     * @return le nombre total d'heures "équivalent TD" prévues pour cet enseignant, arrondi à l'entier le plus proche
     */
    public int heuresPrevues() {
        float result = 0f;
        // On parcour l'ensemble des clés (UE)
        for (UE ue : enseignements.keySet()) {
            result += heuresPrevuesPourUE(ue);
        }
        return Math.round(result);
    }

    /**
     * Calcule le nombre total d'heures prévues pour cet enseignant dans l'UE spécifiée en "heures équivalent TD" Pour le
     * calcul : 1 heure de cours magistral vaut 1,5 h "équivalent TD" 1 heure de TD vaut 1h "équivalent TD" 1 heure de TP vaut
     * 0,75h "équivalent TD"
     *
     * @param ue l'UE concernée
     * @return le nombre total d'heures "équivalent TD" prévues pour cet enseignant arrondi à l'entier le plus proche
     */
    public int heuresPrevuesPourUE(UE ue) {
        float result = 0;

        ServicePrevu p = enseignements.get(ue);
        if (p != null) { // La clé existe, l'enseignant intervient dans l'UE
            result = equivalentTD(TypeIntervention.CM, p.getVolumeCM())
                    + equivalentTD(TypeIntervention.TD, p.getVolumeTD())
                    + equivalentTD(TypeIntervention.TP, p.getVolumeTP())
            ;
        }
        return Math.round(result);
    }

    /**
     * Ajoute un enseignement au service prévu pour cet enseignant
     *
     * @param ue       l'UE concernée
     * @param volumeCM le volume d'heures de cours magitral (positif ou 0)
     * @param volumeTD le volume d'heures de TD (positif ou 0)
     * @param volumeTP le volume d'heures de TP (positif ou 0)
     */
    public void ajouteEnseignement(UE ue, int volumeCM, int volumeTD, int volumeTP) {
        if (volumeCM < 0 || volumeTD < 0 || volumeTP < 0) {
            throw new IllegalArgumentException("Les valeurs doivent être positives ou nulles");
        }

        ServicePrevu s = enseignements.get(ue);
        if (s == null) { // Pas encore de service prévu pour cette UE
            s = new ServicePrevu(volumeCM, volumeTD, volumeTP, ue);
            enseignements.put(ue, s);
        } else { // Mette à jour le service prévu pour cette UE
            s.setVolumeCM(volumeCM + s.getVolumeCM());
            s.setVolumeTD(volumeTD + s.getVolumeTD());
            s.setVolumeTP(volumeTP + s.getVolumeTP());
        }
    }

    /**
     * @return les UE dans lequel cet enseignant doit intervenir
     */
    public Set<UE> UEPrevues() {
        // on renvoie l'ensemble des clés de la Map
        return enseignements.keySet();
    }

    public int resteAPlanifier(UE ue, TypeIntervention type) {
        float planifiees = 0f;
        ServicePrevu p = enseignements.get(ue);
        if (null == p) // L'enseignant n'a pas de service prévudans cette UE
            return 0;

        float aPlanifier = p.getVolumePour(type);

        for (Intervention inter : planification) {
            if ((ue.equals(inter.getMatiere())) && (type.equals(inter.getType()))) {
                planifiees += inter.getDuree();
            }
        }
        return Math.round(aPlanifier - planifiees);
    }

}
