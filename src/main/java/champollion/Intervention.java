package champollion;

import java.util.Date;

/**
 * @author rbastide
 */
public class Intervention {

    private int duree;
    private Date date;
    private Enseignant intervenant;
    private UE matiere;
    private TypeIntervention type;
    private Salle lieu;

    public Intervention(int duree, Date date, Enseignant intervenant, UE matiere, TypeIntervention type, Salle lieu) {
        this.duree = duree;
        this.date = date;
        this.intervenant = intervenant;
        this.matiere = matiere;
        this.type = type;
        this.lieu = lieu;
    }

    /**
     * Get the value of duree
     *
     * @return the value of duree
     */
    public int getDuree() {
        return duree;
    }

    /**
     * Set the value of duree
     *
     * @param duree new value of duree
     */
    public void setDuree(int duree) {
        this.duree = duree;
    }

    /**
     * Get the value of date
     *
     * @return the value of date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Set the value of date
     *
     * @param date new value of date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Get the value of intervenant
     *
     * @return the value of intervenant
     */
    public Enseignant getIntervenant() {
        return intervenant;
    }

    /**
     * Set the value of intervenant
     *
     * @param intervenant new value of intervenant
     */
    public void setIntervenant(Enseignant intervenant) {
        this.intervenant = intervenant;
    }

    /**
     * Get the value of matiere
     *
     * @return the value of matiere
     */
    public UE getMatiere() {
        return matiere;
    }

    /**
     * Set the value of matiere
     *
     * @param matiere new value of matiere
     */
    public void setMatiere(UE matiere) {
        this.matiere = matiere;
    }

    /**
     * Get the value of type
     *
     * @return the value of type
     */
    public TypeIntervention getType() {
        return type;
    }

    /**
     * Set the value of type
     *
     * @param type new value of type
     */
    public void setType(TypeIntervention type) {
        this.type = type;
    }

    /**
     * Get the value of lieu
     *
     * @return the value of lieu
     */
    public Salle getLieu() {
        return lieu;
    }

    /**
     * Set the value of lieu
     *
     * @param lieu new value of lieu
     */
    public void setLieu(Salle lieu) {
        this.lieu = lieu;
    }


}
