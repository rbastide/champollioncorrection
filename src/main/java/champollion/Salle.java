package champollion;

/**
 * @author rbastide
 */
public class Salle {

    private String intitule;

    private int capacite;

    public Salle(String intitule, int capacite) {
        this.intitule = intitule;
        this.capacite = capacite;
    }

    @Override
    public String toString() {
        return "Salle{" + "intitule=" + intitule + ", capacite=" + capacite + '}';
    }

    /**
     * Get the value of capacite
     *
     * @return the value of capacite
     */
    public int getCapacite() {
        return capacite;
    }

    /**
     * Set the value of capacite
     *
     * @param capacite new value of capacite
     */
    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    /**
     * Get the value of intitule
     *
     * @return the value of intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Set the value of intitule
     *
     * @param intitule new value of intitule
     */
    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

}
